package ru.trifonov.tm.terminal;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.bootstrap.Bootstrap;

public abstract class AbstractCommand {
    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;
}
