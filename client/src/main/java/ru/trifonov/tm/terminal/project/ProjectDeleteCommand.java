package ru.trifonov.tm.terminal.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.IProjectEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class ProjectDeleteCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IProjectEndpoint projectEndpoint;

    @Inject
    public ProjectDeleteCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IProjectEndpoint projectEndpoint
    ) {
        this.terminalService = terminalService;
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT DELETE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the project you want delete");
        @Nullable final String id = terminalService.getInCommand();
        projectEndpoint.deleteProject(currentSession, id);
        System.out.println("[OK]");
    }
}
