package ru.trifonov.tm.terminal.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.IDomainEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class DomainSaveJaxbJSON extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IDomainEndpoint domainEndpoint;

    @Inject
    public DomainSaveJaxbJSON(
            @NotNull final TerminalService terminalService,
            @NotNull final IDomainEndpoint domainEndpoint
    ) {
        this.terminalService = terminalService;
        this.domainEndpoint = domainEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "domain-saveJaxJS";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": save domain to json with jax-b";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN SAVE OF JAXB JSON]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        domainEndpoint.domainSaveJaxbJSON(currentSession);
        System.out.println("[OK]");
    }
}
