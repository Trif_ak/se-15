package ru.trifonov.tm.terminal.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public final class ProjectSortCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IProjectEndpoint projectEndpoint;

    @Inject
    public ProjectSortCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IProjectEndpoint projectEndpoint
    ) {
        this.terminalService = terminalService;
        this.projectEndpoint = projectEndpoint;
    }

    @Override
    public @NotNull String getName() {
        return "project-sort";
    }

    @Override
    public @NotNull String getDescription() {
        return ": sort your projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SORT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Select the sort type: \n date-create \n date-begin \n date-end \n status");
        @Nullable final String comparatorName = terminalService.getInCommand();
        @NotNull final List<ProjectDTO> projects = projectEndpoint.sortByProject(currentSession, comparatorName);
        for (@NotNull final ProjectDTO project : projects) {
            System.out.print("  NAME PROJECT " + project.getName());
            System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
            System.out.println("  ID PROJECT " + project.getId());
        }
        System.out.println("[OK]");
    }
}
