package ru.trifonov.tm.terminal.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;


import javax.inject.Inject;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public final class ProjectGetByPartStringCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IProjectEndpoint projectEndpoint;

    @Inject
    public ProjectGetByPartStringCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IProjectEndpoint projectEndpoint
    ) {
        this.terminalService = terminalService;
        this.projectEndpoint = projectEndpoint;
    }

    @Override
    public @NotNull String getName() {
        return "project-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter part of title or description");
        @Nullable final String partString = terminalService.getInCommand();
        @NotNull final List<ProjectDTO> projects = projectEndpoint.getProjectByPartString(currentSession, partString);
        for (@NotNull final ProjectDTO project : projects) {
            System.out.print("  NAME PROJECT " + project.getName());
            System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
            System.out.println("  ID PROJECT " + project.getId());
        }
        System.out.println("[OK]");
    }
}
