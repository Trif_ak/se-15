package ru.trifonov.tm.terminal.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;


import javax.inject.Inject;
import java.lang.Exception;
import java.util.Collection;
import java.util.List;

@NoArgsConstructor
public final class ProjectGetAllCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IProjectEndpoint projectEndpoint;

    @Inject
    public ProjectGetAllCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IProjectEndpoint projectEndpoint
    ) {
        this.terminalService = terminalService;
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL PROJECTS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println(currentSession.getSignature() + currentSession.getUserId());
        @NotNull final List<ProjectDTO> inputList = projectEndpoint.getAllProjectOfUser(currentSession);
        for (@NotNull final ProjectDTO project : inputList) {
            System.out.print("  NAME PROJECT " + project.getName());
            System.out.print("  DESCRIPTION PROJECT " + project.getDescription());
            System.out.println("  ID PROJECT " + project.getId());
        }
        System.out.println("[OK]");
    }
}
