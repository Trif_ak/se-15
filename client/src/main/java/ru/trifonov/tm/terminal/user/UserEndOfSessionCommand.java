package ru.trifonov.tm.terminal.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.ISessionEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class UserEndOfSessionCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ISessionEndpoint sessionEndpoint;

    @Inject
    public UserEndOfSessionCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ISessionEndpoint sessionEndpoint
    ) {
        this.terminalService = terminalService;
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": end of user session";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGOUT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Log in to log out");
        sessionEndpoint.closeSession(currentSession);
        terminalService.setCurrentSession(null);
        System.out.println("[OK]");
    }
}
