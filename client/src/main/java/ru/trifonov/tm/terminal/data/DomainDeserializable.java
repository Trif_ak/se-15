package ru.trifonov.tm.terminal.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.IDomainEndpoint;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class DomainDeserializable extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IDomainEndpoint domainEndpoint;

    @Inject
    public DomainDeserializable(
            @NotNull final TerminalService terminalService,
            @NotNull final IDomainEndpoint domainEndpoint
    ) {
        this.terminalService = terminalService;
        this.domainEndpoint = domainEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "domain-des";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": deserializable domain";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN DESERIALIZABLE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        domainEndpoint.domainSerializable(currentSession);
        System.out.println("[OK]");
    }
}
