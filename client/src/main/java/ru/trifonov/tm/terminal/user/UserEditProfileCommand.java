package ru.trifonov.tm.terminal.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class UserEditProfileCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IUserEndpoint userEndpoint;

    @Inject
    public UserEditProfileCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IUserEndpoint userEndpoint
    ) {
        this.terminalService = terminalService;
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": edit user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER EDIT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter id user who you want to edit");
        @Nullable final String userId = terminalService.getInCommand();
        System.out.println("Enter new login");
        @Nullable final String login = terminalService.getInCommand();
        System.out.println("Enter new password");
        @Nullable final String password = terminalService.getInCommand();
        userEndpoint.updateUser(currentSession, userId, login, password);
        System.out.println("[OK]");
    }
}
