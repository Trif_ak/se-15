package ru.trifonov.tm.terminal.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;
import java.lang.Exception;
import java.util.List;

@NoArgsConstructor
public final class TaskGetByPartStringCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ITaskEndpoint taskEndpoint;

    @Inject
    public TaskGetByPartStringCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ITaskEndpoint taskEndpoint
    ) {
        this.terminalService = terminalService;
        this.taskEndpoint = taskEndpoint;
    }

    @Override
    public @NotNull String getName() {
        return "task-findPart";
    }

    @Override
    public @NotNull String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT BY PART TITLE OR DESCRIPTION]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        System.out.println("Enter part of title or description");
        @Nullable final String partString = terminalService.getInCommand();
        @NotNull final List<TaskDTO> tasks =
                taskEndpoint.getTaskByPartString(currentSession, projectId, partString);
        for (@NotNull final TaskDTO task : tasks) {
            System.out.print("  NAME TASK " + task.getName());
            System.out.print("  DESCRIPTION TASK " + task.getDescription());
            System.out.println("  ID TASK " + task.getId());
        }
        System.out.println("[OK]");
    }
}
