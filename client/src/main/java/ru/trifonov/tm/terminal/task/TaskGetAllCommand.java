package ru.trifonov.tm.terminal.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;


import javax.inject.Inject;
import java.lang.Exception;
import java.util.Collection;

@NoArgsConstructor
public final class TaskGetAllCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ITaskEndpoint taskEndpoint;

    @Inject
    public TaskGetAllCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ITaskEndpoint taskEndpoint
    ) {
        this.terminalService = terminalService;
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": get all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL TASKS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        @NotNull final Collection<TaskDTO> inputList =
                taskEndpoint.getAllTaskOfProject(currentSession, projectId);
        for (@NotNull final TaskDTO task : inputList) {
            System.out.print("  NAME TASK " + task.getName());
            System.out.print("  DESCRIPTION TASK " + task.getDescription());
            System.out.println("  ID TASK " + task.getId());
        }
        System.out.println("[OK]");
    }
}
