package ru.trifonov.tm.terminal.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.Exception_Exception;
import ru.trifonov.tm.endpoint.ISessionEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class UserAuthorizationCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ISessionEndpoint sessionEndpoint;

    @Inject
    public UserAuthorizationCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ISessionEndpoint sessionEndpoint
    ) {
        this.terminalService = terminalService;
        this.sessionEndpoint = sessionEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": authorization in program";
    }

    @Override
    public void execute() throws Exception_Exception {
        System.out.println("[USER AUTHORIZATION]");
        System.out.println(terminalService);
        System.out.println(sessionEndpoint);
        @Nullable SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession != null) throw new NullPointerException("LOG OUT to LOG IN");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = terminalService.getInCommand();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = terminalService.getInCommand();
        currentSession = sessionEndpoint.openSession(login, password);
        if (currentSession == null) throw new NullPointerException("Not found user. Please, registration");
        terminalService.setCurrentSession(currentSession);
        System.out.println("[OK]");
    }
}
