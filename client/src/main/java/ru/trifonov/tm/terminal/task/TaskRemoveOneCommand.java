package ru.trifonov.tm.terminal.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class TaskRemoveOneCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ITaskEndpoint taskEndpoint;

    @Inject
    public TaskRemoveOneCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ITaskEndpoint taskEndpoint
    ) {
        this.terminalService = terminalService;
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK DELETE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the task you want to delete");
        @Nullable final String id = terminalService.getInCommand();
        taskEndpoint.getTask(currentSession, id);
        System.out.println("[OK]");
    }
}
