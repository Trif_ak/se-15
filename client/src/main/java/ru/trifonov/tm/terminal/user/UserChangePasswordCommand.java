package ru.trifonov.tm.terminal.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.ISessionEndpoint;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class UserChangePasswordCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IUserEndpoint userEndpoint;

    @Inject
    public UserChangePasswordCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IUserEndpoint userEndpoint
    ) {
        this.terminalService = terminalService;
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-changePas";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PASSWORD CHANGE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter new password");
        @Nullable final String newPassword = terminalService.getInCommand();
        userEndpoint.changePassword(currentSession, newPassword);
        System.out.println("[OK]");
    }
}