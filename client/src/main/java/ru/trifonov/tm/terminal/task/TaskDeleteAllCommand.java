package ru.trifonov.tm.terminal.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class TaskDeleteAllCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ITaskEndpoint taskEndpoint;

    @Inject
    public TaskDeleteAllCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ITaskEndpoint taskEndpoint
    ) {
        this.terminalService = terminalService;
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-deleteAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": delete select tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE ALL TASKS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter ID of project");
        @Nullable final String projectId = terminalService.getInCommand();
        taskEndpoint.deleteAllTaskOfProject(currentSession, projectId);
        System.out.println("[OK]");
    }

}
