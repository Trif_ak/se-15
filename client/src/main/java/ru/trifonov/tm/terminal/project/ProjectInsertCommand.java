package ru.trifonov.tm.terminal.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.IProjectEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class ProjectInsertCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IProjectEndpoint projectEndpoint;

    @Inject
    public ProjectInsertCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IProjectEndpoint projectEndpoint
    ) {
        this.terminalService = terminalService;
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-insert";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT INSERT]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter name");
        @Nullable final String name = terminalService.getInCommand();
        System.out.println("Enter description");
        @Nullable final String description = terminalService.getInCommand();
        System.out.println("Enter start date. Date format DD.MM.YYYY");
        @Nullable final String beginDate = terminalService.getInCommand();
        System.out.println("Enter finish date. Date format DD.MM.YYYY");
        @Nullable final String endDate = terminalService.getInCommand();
        projectEndpoint.insertProject(currentSession, name, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
