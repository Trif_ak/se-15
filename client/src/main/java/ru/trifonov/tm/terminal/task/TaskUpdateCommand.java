package ru.trifonov.tm.terminal.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ITaskEndpoint taskEndpoint;

    @Inject
    public TaskUpdateCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ITaskEndpoint taskEndpoint
    ) {
        this.terminalService = terminalService;
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": update select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the task you want to update");
        @Nullable final String id = terminalService.getInCommand();
        System.out.println("Enter new name");
        @Nullable final String name = terminalService.getInCommand();
        System.out.println("Enter new projectId");
        @Nullable final String projectId = terminalService.getInCommand();
        System.out.println("Enter new description");
        @Nullable final String description = terminalService.getInCommand();
        System.out.println("Enter new start date task. Date format DD.MM.YYYY");
        @Nullable final String beginDate = terminalService.getInCommand();
        System.out.println("Enter new finish date task. Date format DD.MM.YYYY");
        @Nullable final String endDate = terminalService.getInCommand();
        taskEndpoint.updateTask(currentSession, name, id, projectId, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
