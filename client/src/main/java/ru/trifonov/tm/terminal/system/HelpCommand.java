package ru.trifonov.tm.terminal.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;

    @Inject
    public HelpCommand(@NotNull final TerminalService terminalService) {
        this.terminalService = terminalService;
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": show all COMMANDS";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL COMMANDS]");
        for (@NotNull final AbstractCommand command : terminalService.getCommands().values())
            System.out.printf("%-25s > %s \n", command.getName(), command.getDescription());
    }
}
