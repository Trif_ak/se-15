package ru.trifonov.tm.terminal.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class UserRegistrationCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IUserEndpoint userEndpoint;

    @Inject
    public UserRegistrationCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IUserEndpoint userEndpoint
    ) {
        this.terminalService = terminalService;
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": registration new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession != null) throw new NullPointerException("LOG OUT to REGISTRATION");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = terminalService.getInCommand();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = terminalService.getInCommand();
        userEndpoint.registrationUser(login, password);
        System.out.println("[OK]");
    }
}
