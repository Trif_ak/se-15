package ru.trifonov.tm.terminal.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;
import java.lang.Exception;
import java.util.Collection;

@NoArgsConstructor
public final class UserGetAllCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IUserEndpoint userEndpoint;

    @Inject
    public UserGetAllCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IUserEndpoint userEndpoint
    ) {
        this.terminalService = terminalService;
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-getAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET ALL USERS]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final Collection<UserDTO> inputList = userEndpoint.getAllUser(currentSession);
        for (@NotNull final UserDTO userDTO : inputList) {
            System.out.print("  LOGIN USER " + userDTO.getLogin());
            System.out.print("  PASSWORD USER " + userDTO.getPasswordHash());
            System.out.println("  ID USER " + userDTO.getId());
        }
        System.out.println("[OK]");
    }
}
