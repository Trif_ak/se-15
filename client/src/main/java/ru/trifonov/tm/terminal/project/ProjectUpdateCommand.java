package ru.trifonov.tm.terminal.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.IProjectEndpoint;
import ru.trifonov.tm.endpoint.Session;
import ru.trifonov.tm.endpoint.SessionDTO;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;

@NoArgsConstructor
public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IProjectEndpoint projectEndpoint;

    @Inject
    public ProjectUpdateCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IProjectEndpoint projectEndpoint
    ) {
        this.terminalService = terminalService;
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": update select project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT UPDATE]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the project you want to update");
        @Nullable final String id = terminalService.getInCommand();
        System.out.println("Enter new name");
        @Nullable final String name = terminalService.getInCommand();
        System.out.println("Enter new description");
        @Nullable final String description = terminalService.getInCommand();
        System.out.println("Enter new begin date. Date format DD.MM.YYYY");
        @Nullable final String beginDate = terminalService.getInCommand();
        System.out.println("Enter new end date. Date format DD.MM.YYYY");
        @Nullable final String endDate = terminalService.getInCommand();
        projectEndpoint.updateProject(currentSession, name, id, description, beginDate, endDate);
        System.out.println("[OK]");
    }
}
