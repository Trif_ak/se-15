package ru.trifonov.tm.terminal.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;
import java.lang.Exception;

@NoArgsConstructor
public final class UserGetCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private IUserEndpoint userEndpoint;

    @Inject
    public UserGetCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final IUserEndpoint userEndpoint
    ) {
        this.terminalService = terminalService;
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET USER]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        @NotNull final UserDTO userDTO = userEndpoint.getUser(currentSession);
        System.out.print("  LOGIN USER " + userDTO.getLogin());
        System.out.print("  PASSWORD USER " + userDTO.getPasswordHash());
        System.out.println("  ID USER " + userDTO.getId());
        System.out.println("[OK]");
    }
}
