package ru.trifonov.tm.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.SessionDTO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

@Getter
@Setter
@Singleton
public class TerminalService {
    @NotNull private final Scanner inCommand;
    @Nullable private SessionDTO currentSession;

    @Inject
    public TerminalService(@NotNull Scanner inCommand) {
        this.inCommand = inCommand;
    }

    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public String getInCommand() {
        return inCommand.nextLine();
    }

}
