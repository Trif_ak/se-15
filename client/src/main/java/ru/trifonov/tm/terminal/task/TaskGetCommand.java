package ru.trifonov.tm.terminal.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.endpoint.*;
import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.inject.Inject;
import java.lang.Exception;

@NoArgsConstructor
public final class TaskGetCommand extends AbstractCommand {
    @NotNull private TerminalService terminalService;
    @NotNull private ITaskEndpoint taskEndpoint;

    @Inject
    public TaskGetCommand(
            @NotNull final TerminalService terminalService,
            @NotNull final ITaskEndpoint taskEndpoint
    ) {
        this.terminalService = terminalService;
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String getName() {
        return "task-get";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[GET TASK]");
        @Nullable final SessionDTO currentSession = terminalService.getCurrentSession();
        if (currentSession == null) throw new NullPointerException("Please, LOG IN");
        System.out.println("Enter the ID of the task");
        @Nullable final String id = terminalService.getInCommand();
        @NotNull final TaskDTO task = taskEndpoint.getTask(currentSession, id);
        System.out.print("  NAME TASK " + task.getName());
        System.out.print("  DESCRIPTION TASK " + task.getDescription());
        System.out.println("  ID TASK " + task.getId());
        System.out.println("[OK]");
    }
}
