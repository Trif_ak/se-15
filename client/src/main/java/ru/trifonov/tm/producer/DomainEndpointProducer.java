package ru.trifonov.tm.producer;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.endpoint.DomainEndpointService;
import ru.trifonov.tm.endpoint.IDomainEndpoint;

import javax.enterprise.inject.Produces;

public final class DomainEndpointProducer {
    @NotNull
    @Produces
    public IDomainEndpoint domainEndpoint() {
        return new DomainEndpointService().getDomainEndpointPort();
    }
}
