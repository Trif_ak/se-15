package ru.trifonov.tm.producer;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.endpoint.IUserEndpoint;
import ru.trifonov.tm.endpoint.UserEndpointService;

import javax.enterprise.inject.Produces;

public final class UserEndpointProducer {
    @NotNull
    @Produces
    public IUserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }
}
