package ru.trifonov.tm.producer;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.endpoint.IProjectEndpoint;
import ru.trifonov.tm.endpoint.ProjectEndpointService;

import javax.enterprise.inject.Produces;

public final class ProjectEndpointProducer {
    @NotNull
    @Produces
    public IProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }
}
