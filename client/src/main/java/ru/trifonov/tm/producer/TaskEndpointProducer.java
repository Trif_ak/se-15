package ru.trifonov.tm.producer;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.endpoint.ITaskEndpoint;
import ru.trifonov.tm.endpoint.TaskEndpointService;

import javax.enterprise.inject.Produces;

public final class TaskEndpointProducer {
    @NotNull
    @Produces
    public ITaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }
}
