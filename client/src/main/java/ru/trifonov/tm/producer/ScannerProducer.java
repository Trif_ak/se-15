package ru.trifonov.tm.producer;

import org.jetbrains.annotations.NotNull;

import javax.enterprise.inject.Produces;
import java.util.Scanner;

public final class ScannerProducer {
    @NotNull
    @Produces
    public Scanner scanner() {
        return new Scanner(System.in);
    }
}
