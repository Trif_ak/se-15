package ru.trifonov.tm.producer;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.endpoint.ISessionEndpoint;
import ru.trifonov.tm.endpoint.SessionEndpointService;

import javax.enterprise.inject.Produces;

public final class SessionEndpointProducer {
    @NotNull
    @Produces
    public ISessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }
}
