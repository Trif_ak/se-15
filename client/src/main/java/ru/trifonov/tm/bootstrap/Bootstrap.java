package ru.trifonov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.trifonov.tm.terminal.AbstractCommand;
import ru.trifonov.tm.terminal.TerminalService;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.lang.Exception;

public final class Bootstrap {
    @NotNull private final TerminalService terminalService;
    @Any @NotNull private final Instance<AbstractCommand> abstractCommands;

    @Inject
    public Bootstrap(@NotNull TerminalService terminalService, @NotNull Instance<AbstractCommand> abstractCommands) {
        this.terminalService = terminalService;
        this.abstractCommands = abstractCommands;
    }

    public void start() {
        System.out.println("*** WELCOME TO TASK MANAGER *** \n Enter terminal \"help\" for watch all commands");
        try {
            init();
            while (true) {
                System.out.println("\n Enter command:");
                @NotNull final AbstractCommand abstractCommand =
                        terminalService.getCommands().get(terminalService.getInCommand());
                if (abstractCommand == null) {
                    System.out.println("There is no such command");
                    continue;
                }
                execute(abstractCommand);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void init() {
        terminalService.getCommands().clear();
        for (AbstractCommand command : abstractCommands) {
            registry(command);
        }
    }

    private void registry(AbstractCommand abstractCommand) {
        @NotNull final String nameCommand = abstractCommand.getName();
        terminalService.getCommands().put(nameCommand, abstractCommand);
    }

    private void execute (@Nullable final AbstractCommand abstractCommand) {
        try {
            abstractCommand.execute();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}


