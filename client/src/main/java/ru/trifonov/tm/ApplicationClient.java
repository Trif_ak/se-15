package ru.trifonov.tm;

import ru.trifonov.tm.bootstrap.*;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class ApplicationClient {
    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(ApplicationClient.class).initialize()
                .select(Bootstrap.class).get().start();
    }
}