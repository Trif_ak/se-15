package ru.trifonov.tm.util;

import lombok.experimental.UtilityClass;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.User;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class EntityManagerUtil {
    @Produces
    public static EntityManagerFactory factory() {
        @NotNull final String driver = "com.mysql.jdbc.Driver";
        @NotNull final String url = "jdbc:mysql://127.0.0.1:3306/tm";
        @NotNull final String user = "root";
        @NotNull final String password = "root";

        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, driver);
        settings.put(Environment.URL, url);
        settings.put(Environment.USER, user);
        settings.put(Environment.PASS, password);
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
