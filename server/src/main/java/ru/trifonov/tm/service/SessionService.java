package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ISessionRepository;
import ru.trifonov.tm.api.service.IPropertyService;
import ru.trifonov.tm.api.service.ISessionService;
import ru.trifonov.tm.api.service.IUserService;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.entity.Session;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.repository.SessionRepository;
import ru.trifonov.tm.util.SignatureUtil;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class SessionService extends AbstractService implements ISessionService {
    List<RoleType> roles = Arrays.asList(RoleType.REGULAR_USER, RoleType.ADMIN);
    @NotNull private final EntityManagerFactory managerFactory;
    @NotNull private final IUserService userService;
    @NotNull private final IPropertyService propertyService;

    @Inject
    public SessionService(
            @NotNull EntityManagerFactory managerFactory,
            @NotNull IUserService userService,
            @NotNull IPropertyService propertyService
    ) {
        this.managerFactory = managerFactory;
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void persist(@Nullable final Session session) {
        if (session == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.persist(session);
    }

    @Override
    @NotNull
    public List<SessionDTO> getByUserId(@NotNull final SessionDTO sessionDTO) throws Exception {
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @NotNull final String userId = sessionDTO.getUserId();
        @Nullable List<Session> sessions = sessionRepository.getByUserId(userId);
        if (sessions == null || sessions.isEmpty()) throw new NullPointerException("Sessions not found.");
        @NotNull List<SessionDTO> sessionsDTO = new ArrayList<>();
        for (@NotNull final Session session : sessions) {
            sessionsDTO.add(entityToDTO(session));
        }
        return sessionsDTO;
    }

    @Override
    @NotNull
    public List<Session> getAll() {
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @Nullable List<Session> sessions = sessionRepository.getAll();
        if (sessions == null || sessions.isEmpty()) throw new NullPointerException("Sessions not found.");
        return sessions;
    }

    @Override
    @NotNull
    public SessionDTO openSession (@Nullable final String login, @Nullable final String password) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data.");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data.");
        @Nullable final User checkedUser = userService.existsUser(login, password);
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(checkedUser.getId());
        sessionDTO.setRole(checkedUser.getRoleType());
        @NotNull final String salt = "sdsdsds";
        @NotNull final Integer cycle = 31;
        @Nullable final String signature = SignatureUtil.sign(sessionDTO, salt, cycle);
        if (signature == null || signature.isEmpty()) throw new NullPointerException("Something wrong.");
        sessionDTO.setSignature(signature);
        @NotNull final Session session = dtoToEntity(sessionDTO, checkedUser);
        persist(session);
        return sessionDTO;
    }

    @Override
    public void closeSession(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @NotNull final String id = sessionDTO.getId();
        sessionRepository.delete(id);
    }

    @Override
    public void validateAdmin(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) throw new NullPointerException("Enter correct data");
        if (sessionDTO.getSignature() == null || sessionDTO.getSignature().trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!sessionDTO.getRole().equals(RoleType.ADMIN)) throw new Exception("Invalid session.");
        @NotNull final SessionDTO temp = sessionDTO.clone();
        @NotNull final String signatureSession = sessionDTO.getSignature();
        @Nullable final String signatureTemp = sign(temp).getSignature();
        if (!signatureSession.equals(signatureTemp)) throw new NullPointerException("Enter correct data");
        @NotNull final long timeStampOfSession = sessionDTO.getTimestamp();
        @NotNull final long currentTime = System.currentTimeMillis();
        if (currentTime - timeStampOfSession > 900000) throw new Exception("Session time is out. Please, LOG IN");
    }

    @Override
    public void validate(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) throw new NullPointerException("Enter correct data");
        if (sessionDTO.getSignature() == null || sessionDTO.getSignature().trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (!roles.contains(sessionDTO.getRole())) throw new Exception("Invalid session.");
        @Nullable final SessionDTO temp = sessionDTO.clone();
        @NotNull final String signatureSession = sessionDTO.getSignature();
        @Nullable final String signatureTemp = sign(temp).getSignature();
        if (!signatureSession.equals(signatureTemp)) throw new NullPointerException("Enter correct data");
        @NotNull final long timeStampOfSession = sessionDTO.getTimestamp();
        @NotNull final long currentTime = System.currentTimeMillis();
        if (currentTime - timeStampOfSession > 900000) throw new Exception("Session time is out. Please, LOG IN");
    }

    @Override
    @NotNull
    public SessionDTO sign(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new NullPointerException("Enter correct data");
        sessionDTO.setSignature(null);
        @Nullable final String salt = propertyService.getServerSalt();
        @Nullable final Integer cycle = propertyService.getServerCycle();
        @Nullable final String signature = SignatureUtil.sign(sessionDTO, salt, cycle);
        if (signature == null) throw new NullPointerException("Signature is empty");
        sessionDTO.setSignature(signature);
        return sessionDTO;
    }

    @Override
    public void load(@Nullable final List<Session> sessions) {
        if (sessions == null) return;
        for (@NotNull final Session session : sessions) {
            persist(session);
        }
    }

    @NotNull
    public SessionDTO entityToDTO(@NotNull final Session session) {
        @NotNull final SessionDTO sessionDTO = new SessionDTO(
                session.getId(),
                session.getUser().getId(),
                session.getTimestamp(),
                session.getSignature(),
                session.getRole()
        );
        return sessionDTO;
    }

    @NotNull
    public Session dtoToEntity(@NotNull final SessionDTO sessionDTO, @NotNull final User user) {
        @NotNull final Session session = new Session(
                sessionDTO.getId(),
                user,
                sessionDTO.getTimestamp(),
                sessionDTO.getSignature(),
                sessionDTO.getRole()
        );
        return session;
    }
}
