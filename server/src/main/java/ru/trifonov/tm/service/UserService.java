package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IUserRepository;
import ru.trifonov.tm.api.service.IUserService;
import ru.trifonov.tm.dto.UserDTO;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;
import ru.trifonov.tm.repository.UserRepository;
import ru.trifonov.tm.util.HashUtil;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Singleton
public final class UserService extends ComparatorService implements IUserService {
    @NotNull private EntityManagerFactory managerFactory;

    @Inject
    public UserService(@NotNull final EntityManagerFactory managerFactory) {
        this.managerFactory = managerFactory;
    }

    @Override
    public void persist(@Nullable final User user) {
        if (user == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.persist(user);
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.merge(user);
    }

    @Override
    public void update(
            @Nullable final String id, @Nullable final String login,
            @Nullable final String password, @Nullable final RoleType roleType
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (roleType == null) throw new NullPointerException("Enter correct data");

        @NotNull final String passwordHash = HashUtil.md5(password);
        @NotNull final User admin = new User(id, login, passwordHash, roleType);
        merge(admin);
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.delete(id);
    }

    @Override
    @NotNull
    public List<User> getAll() {
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable List<User> users = userRepository.getAll();
        if (users == null || users.isEmpty()) throw new NullPointerException("Users not found.");
        return users;
    }

    @Override
    @NotNull
    public User get(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable User user = userRepository.get(id);
        if (user == null) throw new NullPointerException("User not found.");
        return user;
    }

    @Override
    public void changePassword(@Nullable final String id, @Nullable final String newPassword) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (newPassword == null || newPassword.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull String newPasswordHash = HashUtil.md5(newPassword);
        userRepository.changePassword(id, newPasswordHash);
    }

    @Override
    public void registrationUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(login, HashUtil.md5(password), RoleType.REGULAR_USER);
        persist(user);
    }

    @Override
    public void registrationAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (getByLogin(login)) throw new NullPointerException("Enter correct login. Login already exists");
        @NotNull final User user = new User(login, HashUtil.md5(password), RoleType.ADMIN);
        persist(user);
    }

    @Override
    public void addUser() throws Exception {
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final User admin = new User();
        admin.setLogin("admin");
        if (getByLogin(admin.getLogin())) throw new Exception("Login already exist.");
        admin.setPasswordHash(HashUtil.md5("admin"));
        admin.setRoleType(RoleType.ADMIN);
        userRepository.persist(admin);

        @NotNull final User user = new User();
        user.setLogin("user");
        if (getByLogin(user.getLogin())) throw new Exception("Login already exist.");
        user.setPasswordHash(HashUtil.md5("user"));
        user.setRoleType(RoleType.REGULAR_USER);
        userRepository.persist(user);
    }

    @Override
    @NotNull
    public User existsUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (password == null || password.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        IUserRepository userRepository = new UserRepository(entityManager);
        @NotNull final String passwordHash = HashUtil.md5(password);
        @Nullable User user = userRepository.existsUser(login, passwordHash);
        if (user == null) throw new NullPointerException("User not exist");
        return user;
    }

    @Override
    public boolean getByLogin(@Nullable final String login) {
        if (login == null || login.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable User user = userRepository.getByLogin(login);
        if (user == null) return false;
        @NotNull final String loginResult = user.getLogin();
        return login.equals(loginResult);
    }

    @Override
    public void load(@Nullable final List<User> users) {
        if (users == null) return;
        for (@NotNull final User user : users) {
            persist(user);
        }
    }

    @Override
    @NotNull
    public UserDTO entityToDTO(@NotNull final User user) {
        @NotNull final UserDTO userDTO = new UserDTO(
                user.getId(),
                user.getLogin(),
                user.getPasswordHash(),
                user.getRoleType()
        );
        return userDTO;
    }
}