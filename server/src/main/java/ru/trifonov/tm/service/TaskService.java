package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ITaskRepository;
import ru.trifonov.tm.api.service.IProjectService;
import ru.trifonov.tm.api.service.IPropertyService;
import ru.trifonov.tm.api.service.ITaskService;
import ru.trifonov.tm.api.service.IUserService;
import ru.trifonov.tm.dto.TaskDTO;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.repository.TaskRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Singleton
public final class TaskService extends ComparatorService implements ITaskService {
    @NotNull private final EntityManagerFactory managerFactory;
    @NotNull private final IUserService userService;
    @NotNull private final IProjectService projectService;

    @Inject
    public TaskService(
            @NotNull final EntityManagerFactory managerFactory,
            @NotNull final IUserService userService,
            @NotNull final IProjectService projectService
    ) {
        this.managerFactory = managerFactory;
        this.userService = userService;
        this.projectService = projectService;
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.persist(task);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.merge(task);
    }

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String projectId,
            @Nullable final String userId, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Project project = projectService.get(projectId, userId);
        @NotNull final User user = userService.get(userId);
        @NotNull final Task task = new Task(project, user, name, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        persist(task);
    }

    @Override
    public void update(
            @Nullable final String name, @Nullable final String id,
            @Nullable final String projectId, @Nullable final String userId,
            @Nullable final String description, @Nullable final String beginDate,
            @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @NotNull final Project project = projectService.get(projectId, userId);
        @NotNull final User user = userService.get(userId);
        @NotNull final Task task = new Task(project, user, name, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        merge(task);
    }

    @Override
    @NotNull public List<Task> getAll() {
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable List<Task> tasks = taskRepository.getAll();
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Task not found.");
        return tasks;
    }

    @Override
    @NotNull public List<Task> getOfProject(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable List<Task> tasks = taskRepository.getOfProject(projectId, userId);
        if (tasks == null || tasks.isEmpty()) throw new NullPointerException("Task not found.");
        return tasks;
    }

    @Override
    @NotNull public List<Task> getByPartString(
            @Nullable final String userId, @Nullable final String projectId, @Nullable final String partString
    ) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @Nullable final List<Task> tasks = getOfProject(projectId, userId);
        if (tasks.isEmpty()) throw new NullPointerException("Tasks not found");

        @Nullable final List<Task> output = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            if (task.getName().contains(partString) || task.getDescription().contains(partString)) {
                output.add(task);
            }
        }
        if (output.isEmpty()) throw new NullPointerException("Tasks not found.");
        return output;
    }

    @Override
    @NotNull public Task get(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final Task task = taskRepository.get(id, userId);
        if (task == null) throw new NullPointerException("Task not found.");
        return task;
    }

    @Override
    public void deleteOfUser(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.deleteOfUser(userId);
    }

    @Override
    public void deleteOfProject(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.deleteOfProject(projectId, userId);
    }

    @Override
    public void delete(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = managerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.delete(id, userId);
    }

    @Override
    @NotNull public List<Task> sortBy(
            @Nullable final String projectId, @Nullable final String userId,
            @Nullable final String comparatorName
    ) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (projectId == null || projectId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Task> tasks = getOfProject(projectId, userId);
        if (tasks.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        for (@NotNull final Task task : tasks) {
            persist(task);
        }
    }

    @Override
    @NotNull
    public TaskDTO entityToDTO(@NotNull final Task task) {
        @NotNull final TaskDTO taskDTO = new TaskDTO(
                task.getId(),
                task.getProject().getId(),
                task.getUser().getId(),
                task.getName(),
                task.getDescription(),
                task.getStatus(),
                task.getBeginDate(),
                task.getEndDate(),
                task.getCreateDate()
        );
        return taskDTO;
    }
}