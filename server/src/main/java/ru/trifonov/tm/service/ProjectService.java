package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IProjectRepository;
import ru.trifonov.tm.api.service.IProjectService;
import ru.trifonov.tm.api.service.IUserService;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.entity.Project;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.repository.ProjectRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends ComparatorService implements IProjectService {
    @NotNull private final EntityManagerFactory entityManagerFactory;
    @NotNull private final IUserService userService;

    @Inject
    public ProjectService(
            @NotNull final EntityManagerFactory entityManagerFactory,
            @NotNull final IUserService userService
    ) {
        this.entityManagerFactory = entityManagerFactory;
        this.userService = userService;
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.persist(project);
    }

    @Override
    public void insert(
            @Nullable final String userId, @Nullable final String name,
            @Nullable final String description, @Nullable final String beginDate,
            @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final User user = userService.get(userId);
        @NotNull final Project project = new Project(user, name, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        persist(project);
    }

    @Override
    public void merge(@Nullable final Project project) {
        if (project == null) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.merge(project);
    }

    @Override
    public void update(
            @Nullable final String id, @Nullable final String userId,
            @Nullable final String name, @Nullable final String description,
            @Nullable final String beginDate, @Nullable final String endDate
    ) throws Exception {
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (beginDate == null || beginDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (endDate == null || endDate.trim().isEmpty()) throw new NullPointerException("Enter correct data");

        @NotNull final User user = userService.get(userId);
        @NotNull final Project project = new Project(id, user, name, description, dateFormat.parse(beginDate), dateFormat.parse(endDate));
        merge(project);
    }

    @Override
    @NotNull
    public List<Project> getAll() {
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable List<Project> projects = projectRepository.getAll();
        if (projects == null || projects.isEmpty()) throw new NullPointerException("Projects not found.");
        return projects;
    }

    @Override
    @NotNull
    public List<Project> getOfUser(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable List<Project> projects = projectRepository.getOfUser(userId);
        if (projects == null || projects.isEmpty()) throw new NullPointerException("Projects not found.");
        return projects;
    }

    @Override
    @NotNull
    public List<Project> getByPartString(@Nullable final String userId, @Nullable final String partString) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (partString == null || partString.isEmpty()) throw new NullPointerException("Enter correct data");

        @Nullable final List<Project> projects = getOfUser(userId);
        if (projects.isEmpty()) throw new NullPointerException("Projects not found.");

        @Nullable final List<Project> output = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            if (project.getName().contains(partString) || project.getDescription().contains(partString)) {
                output.add(project);
            }
        }
        if (output.isEmpty()) throw new NullPointerException("Projects not found.");
        return output;
    }

    @Override
    @NotNull
    public Project get(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable Project project = projectRepository.get(id, userId);
        if (project == null) throw new NullPointerException("Project no found.");
        return project;
    }

    @Override
    public void deleteAll(@Nullable final String userId) {
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.deleteAll(userId);
    }

    @Override
    public void delete(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final EntityManager entityManager = entityManagerFactory.createEntityManager();
        if (entityManager == null) throw new NullPointerException("Something wrong with connection");
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.delete(id, userId);
    }

    @Override
    public List<Project> sortBy(@Nullable final String userId, @Nullable final String comparatorName){
        if (userId == null || userId.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        if (comparatorName == null || comparatorName.trim().isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final List<Project> projects = getOfUser(userId);
        if (projects.isEmpty()) throw new NullPointerException("Enter correct data");
        @Nullable final Comparator comparator = getComparator(comparatorName);
        if (comparator == null) throw new NullPointerException("Enter correct data");
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        for (@NotNull final Project project : projects) {
            persist(project);
        }
    }

    @Override
    @NotNull
    public ProjectDTO entityToDTO(@NotNull final Project project) {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO(
                project.getId(),
                project.getUser().getId(),
                project.getName(),
                project.getDescription(),
                project.getStatus(),
                project.getBeginDate(),
                project.getEndDate(),
                project.getCreateDate()
        );
        return projectDTO;
    }
}