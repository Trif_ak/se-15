package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {
    @NotNull
    private final String APP_PROPERTY = "/application.properties";
    @NotNull
    private final String DBMS_PROPERTY = "/dataBase.properties";
    @NotNull
    private final Properties appProperties = new Properties();
    @NotNull
    private final Properties dataBaseProperties = new Properties();

    @Override
    public void init() throws IOException {
        InputStream inputStreamApp = Properties.class.getResourceAsStream(APP_PROPERTY);
//        System.out.println(inputStreamApp);
        appProperties.load(inputStreamApp);
//        System.out.println(appProperties);
        InputStream inputStreamDBMS = Properties.class.getResourceAsStream(DBMS_PROPERTY);
//        System.out.println(inputStreamDBMS);
        dataBaseProperties.load(inputStreamDBMS);
//        System.out.println(dataBaseProperties);
    }

    @Override
    public String getURL() {
        @NotNull final String propertyURL = dataBaseProperties.getProperty("url");
        System.out.println(dataBaseProperties.getProperty("url"));
        @Nullable final String envURL = System.getProperty("url");
        System.out.println(System.getProperty("url"));
        if(envURL == null)return propertyURL;
        System.out.println("dsd" + propertyURL);
        System.out.println("dsfsd" + envURL);
        return envURL;
    }

    @Override
    public String getDriver() {
        System.out.println(dataBaseProperties.getProperty("driver"));
        return dataBaseProperties.getProperty("driver");
    }

    @Override
    public String getUsername() {
        System.out.println(dataBaseProperties.getProperty("username"));
        return dataBaseProperties.getProperty("username");
    }

    @Override
    public String getPassword() {
        System.out.println(dataBaseProperties.getProperty("password"));
        return dataBaseProperties.getProperty("password");
    }

    @Override
    public String getServerHost() {
        return appProperties.getProperty("server.host");
    }

    @Override
    public String getServerPort() {
        return appProperties.getProperty("server.port");
    }

    @Override
    public String getServerSalt() {
        return appProperties.getProperty("server.salt");
    }

    @Override
    public Integer getServerCycle() {
        System.out.println(appProperties.getProperty("server.cycle"));
        return Integer.parseInt(appProperties.getProperty("server.cycle"));
    }
}
