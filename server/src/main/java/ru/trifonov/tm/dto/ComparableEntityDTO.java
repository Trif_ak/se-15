package ru.trifonov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.enumerate.CurrentStatus;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class ComparableEntityDTO extends AbstractDTO {
    @NotNull
    protected String projectId = "";

    @NotNull
    protected String userId = "";

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    protected CurrentStatus status = CurrentStatus.PLANNED;

    @NotNull
    protected Date beginDate = new Date();

    @NotNull
    protected Date endDate = new Date();

    @NotNull
    protected Date createDate = new Date();

    @NotNull
    protected final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    protected ComparableEntityDTO(
            @NotNull String userId, @NotNull String name, @NotNull String description,
            @NotNull CurrentStatus status, @NotNull Date beginDate, @NotNull Date endDate

    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.status = status;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    protected ComparableEntityDTO(
            @NotNull String projectId, @NotNull String userId, @NotNull String name,
            @NotNull String description, @NotNull CurrentStatus status, @NotNull Date beginDate, @NotNull Date endDate
    ) {
        this.projectId = projectId;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.status = status;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }
}
