package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {
    void persist(@NotNull Session session);
    void delete(@NotNull String id);
    @Nullable Session getSession(@NotNull String id);
    @Nullable List<Session> getByUserId(@NotNull String userId);
    @Nullable List<Session> getAll();
}
