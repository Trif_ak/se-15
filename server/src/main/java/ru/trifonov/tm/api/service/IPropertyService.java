package ru.trifonov.tm.api.service;

import java.io.IOException;

public interface IPropertyService {
    void init() throws IOException;

    String getURL();

    String getDriver();

    String getUsername();

    String getPassword();

    String getServerHost();
    String getServerPort();
    String getServerSalt();
    Integer getServerCycle();
}
