package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);

    void merge(@NotNull User user);

//    void update(@NotNull String id, @NotNull String login, @NotNull String passwordHash, @NotNull RoleType roleType);

    @Nullable List<User> getAll();

    @Nullable User getByLogin(@NotNull String login);

    @Nullable User get(@NotNull String id);

    void delete(@NotNull String id);

    @Nullable User existsUser(@NotNull String login, @NotNull String passwordHash);

    void changePassword(@NotNull String id, @NotNull String newPasswordHash);
}
