package ru.trifonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
    void persist(@NotNull Project project);
    void merge(@NotNull Project project);
    @Nullable List<Project> getAll();
    @Nullable List<Project> getOfUser(@NotNull String userId);
    @Nullable Project get(@NotNull String id, @NotNull String userId);
    void deleteAll(@NotNull String userId);
    void delete(@NotNull String id, @NotNull String userId);
}
