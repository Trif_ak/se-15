package ru.trifonov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.service.ISessionService;
import ru.trifonov.tm.api.service.ITaskService;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.dto.TaskDTO;
import ru.trifonov.tm.entity.Task;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trifonov.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {
    @NotNull private ISessionService sessionService;
    @NotNull private ITaskService taskService;

    @Inject
    public TaskEndpoint(@NotNull final ISessionService sessionService, @NotNull final ITaskService taskService) {
        this.sessionService = sessionService;
        this.taskService = taskService;
    }

    @Override
    @WebMethod
    public void initTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        taskService.insert(name, projectId, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public void updateTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception, ParseException {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        taskService.update(name, id, projectId, userId, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public TaskDTO getTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final Task task = taskService.get(id, userId);
        return taskService.entityToDTO(task);
    }

    @Override
    @WebMethod
    public List<TaskDTO> getAllTaskOfProject (
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final List<Task> tasks = taskService.getOfProject(projectId, userId);
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(taskService.entityToDTO(task));
        }
        return tasksDTO;
    }

    @Override
    @WebMethod
    public void deleteTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        taskService.delete(id, userId);
    }

    @Override
    @WebMethod
    public void deleteAllTaskOfProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        taskService.deleteOfProject(projectId, userId);
    }

    @Override
    @WebMethod
    public void deleteAllTaskOfUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        taskService.deleteOfUser(userId);
    }

    @Override
    @WebMethod
    public List<TaskDTO> sortByTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull final String comparatorName
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final List<Task> tasks = taskService.sortBy(projectId, userId, comparatorName);
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(taskService.entityToDTO(task));
        }
        return tasksDTO;
    }

    @Override
    @WebMethod
    public List<TaskDTO> getTaskByPartString(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @NotNull final String projectId,
            @WebParam(name = "partString", partName = "partString") @NotNull final String partString
    ) throws Exception {
        sessionService.validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final List<Task> tasks = taskService.getByPartString(userId, projectId, partString);
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(taskService.entityToDTO(task));
        }
        return tasksDTO;
    }

    @Override
    @WebMethod
    public List<TaskDTO> getAllTask(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO session
    ) throws Exception {
        sessionService.validateAdmin(session);
        @NotNull final List<Task> tasks = taskService.getAll();
        @NotNull final List<TaskDTO> tasksDTO = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            tasksDTO.add(taskService.entityToDTO(task));
        }
        return tasksDTO;
    }
}
