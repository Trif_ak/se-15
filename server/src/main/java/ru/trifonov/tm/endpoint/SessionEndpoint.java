package ru.trifonov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.service.ISessionService;
import ru.trifonov.tm.dto.SessionDTO;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trifonov.tm.endpoint.ISessionEndpoint")
public final class SessionEndpoint implements ISessionEndpoint {
    @NotNull private ISessionService sessionService;

    @Inject
    public SessionEndpoint(@NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        return sessionService.openSession(login, password);
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        sessionService.closeSession(sessionDTO);
    }
}