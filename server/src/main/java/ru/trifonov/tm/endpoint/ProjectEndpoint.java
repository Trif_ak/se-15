package ru.trifonov.tm.endpoint;

import com.google.inject.Inject;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.api.service.IProjectService;
import ru.trifonov.tm.api.service.ISessionService;
import ru.trifonov.tm.dto.ProjectDTO;
import ru.trifonov.tm.dto.SessionDTO;
import ru.trifonov.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.trifonov.tm.endpoint.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {
    @NotNull private ISessionService sessionService;
    @NotNull private IProjectService projectService;

    @Inject
    public ProjectEndpoint(@NotNull final ISessionService sessionService, @NotNull final IProjectService projectService) {
        this.sessionService = sessionService;
        this.projectService = projectService;
    }

    @Override
    @WebMethod
    public void insertProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        projectService.insert(userId, name, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public void updateProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") @NotNull final String name,
            @WebParam(name = "id", partName = "id") @NotNull final String id,
            @WebParam(name = "description", partName = "description") @NotNull final String description,
            @WebParam(name = "beginDate", partName = "beginDate") @NotNull final String beginDate,
            @WebParam(name = "endDate", partName = "endDate") @NotNull final String endDate
    ) throws Exception {
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        projectService.update(id, userId, name, description, beginDate, endDate);
    }

    @Override
    @WebMethod
    public ProjectDTO getProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final Project project = projectService.get(id, userId);
        return projectService.entityToDTO(project);
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getAllProjectOfUser(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) throws Exception {
        System.out.println(sessionService);
        System.out.println(projectService);
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final List<Project> projects = projectService.getOfUser(userId);
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(projectService.entityToDTO(project));
        }
        return projectsDTO;
    }

    @Override
    @WebMethod
    public void deleteProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @NotNull final String id
    ) throws Exception {
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        projectService.delete(id, userId);
    }

    @Override
    @WebMethod
    public void deleteAllProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        projectService.deleteAll(userId);
    }

    @Override
    @WebMethod
    public List<ProjectDTO> sortByProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "comparatorName", partName = "comparatorName") @NotNull final String comparatorName
    ) throws Exception {
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final List<Project> projects = projectService.sortBy(userId, comparatorName);
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(projectService.entityToDTO(project));
        }
        return projectsDTO;
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getProjectByPartString(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "partString", partName = "partString") @NotNull final String partString
    ) throws Exception {
        sessionService.validate(sessionDTO);
        @NotNull final String userId = sessionDTO.getUserId();
        @NotNull final List<Project> projects = projectService.getByPartString(userId, partString);;
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(projectService.entityToDTO(project));
        }
        return projectsDTO;
    }

    @Override
    @WebMethod
    public List<ProjectDTO> getAllProject(
            @WebParam(name = "session", partName = "session") @NotNull SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validateAdmin(sessionDTO);
        @NotNull final List<Project> projects = projectService.getAll();
        @NotNull final List<ProjectDTO> projectsDTO = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectsDTO.add(projectService.entityToDTO(project));
        }
        return projectsDTO;
    }
}