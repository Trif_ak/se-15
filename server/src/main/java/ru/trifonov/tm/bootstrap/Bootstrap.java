package ru.trifonov.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.service.*;
import ru.trifonov.tm.endpoint.*;

import javax.inject.Inject;
import javax.xml.ws.Endpoint;
import java.io.IOException;

@Getter
@Setter
public final class Bootstrap {
    @NotNull private final IPropertyService propertyService;
    @NotNull private final IUserService userService;
    @NotNull private final ISessionEndpoint sessionEndpoint;
    @NotNull private final IUserEndpoint userEndpoint;
    @NotNull private final IProjectEndpoint projectEndpoint;
    @NotNull private final ITaskEndpoint taskEndpoint;
    @NotNull private final IDomainEndpoint domainEndpoint;

    @Inject
    public Bootstrap(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService,
            @NotNull final ISessionEndpoint sessionEndpoint,
            @NotNull final IUserEndpoint userEndpoint,
            @NotNull final IProjectEndpoint projectEndpoint,
            @NotNull final ITaskEndpoint taskEndpoint,
            @NotNull final IDomainEndpoint domainEndpoint
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.sessionEndpoint = sessionEndpoint;
        this.userEndpoint = userEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.taskEndpoint = taskEndpoint;
        this.domainEndpoint = domainEndpoint;
    }

    public void start() {
        try {
            init();
        } catch (Exception e) {
            System.err.println(e.getMessage());
//            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        initProperty();
        initEndpoint();
    }

    private void initProperty() throws IOException {
        propertyService.init();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        publisher(wsdl, endpoint);
    }

    private void publisher(@NotNull final String wsdl, @NotNull final Object endpoint) {
        Endpoint.publish(wsdl, endpoint);
    }
}