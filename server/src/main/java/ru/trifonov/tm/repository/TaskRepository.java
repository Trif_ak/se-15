package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.ITaskRepository;
import ru.trifonov.tm.entity.Task;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository implements ITaskRepository {
    @NotNull
    private final EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull Task task) {
        entityManager.getTransaction().begin();
        entityManager.persist(task);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull Task task) {
        entityManager.getTransaction().begin();
        entityManager.merge(task);
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable List<Task> getAll() {
        return entityManager
                .createQuery("SELECT t FROM Task t", Task.class)
                .getResultList();
    }

    @Override
    public @Nullable List<Task> getOfProject(@NotNull String projectId, @NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.project.id = ?1 AND t.user.id = ?2", Task.class)
                .setParameter(1, projectId)
                .setParameter(2, userId)
                .getResultList();
    }

    @Override
    public @Nullable List<Task> getOfUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = ?1", Task.class)
                .setParameter(1, userId)
                .getResultList();
    }

    @Override
    public @Nullable Task get(@NotNull String id, @NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.id = ?1 AND t.user.id = ?2", Task.class)
                .setParameter(1, id)
                .setParameter(2, userId)
                .getSingleResult();
    }

    @Override
    public void deleteOfUser(@NotNull String userId) {
        entityManager.remove(getOfUser(userId));
    }

    @Override
    public void deleteOfProject(@NotNull String projectId, @NotNull String userId) {
        entityManager.remove(getOfProject(projectId, userId));
    }

    @Override
    public void delete(@NotNull String id, @NotNull String userId) {
        entityManager.remove(get(id, userId));
    }
}
