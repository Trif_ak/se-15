package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IProjectRepository;
import ru.trifonov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {
    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull Project project) {
        entityManager.getTransaction().begin();
        entityManager.persist(project);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull Project project) {
        entityManager.getTransaction().begin();
        entityManager.merge(project);
        entityManager.getTransaction().commit();
    }

    @Override
    @Nullable
    public List<Project> getAll() {
        return entityManager
                .createQuery("SELECT p FROM Project p", Project.class)
                .getResultList();
    }

    @Override
    @Nullable
    public List<Project> getOfUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.user.id = ?1", Project.class)
                .setParameter(1, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Project get(@NotNull String id, @NotNull String userId) {
        return entityManager
                .createQuery("SELECT p FROM Project p WHERE p.id = ?1 AND p.user.id = ?2", Project.class)
                .setParameter(1, id)
                .setParameter(2, userId)
                .getSingleResult();
    }

    @Override
    public void deleteAll(@NotNull String userId) {
        @Nullable final List<Project> projects = getOfUser(userId);
        if (projects == null || projects.isEmpty()) return;
        entityManager.getTransaction().begin();
        for (@NotNull final Project project : projects) {
            entityManager.remove(project);
        }
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(@NotNull String id, @NotNull String userId) {
        @Nullable final Project project = get(id, userId);
        if (project == null) return;
        entityManager.getTransaction().begin();
        entityManager.remove(project);
        entityManager.getTransaction().commit();
    }
}
