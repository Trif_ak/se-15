package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.api.repository.IUserRepository;import ru.trifonov.tm.entity.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository implements IUserRepository {
    @NotNull
    private final EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull final User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public void merge(@NotNull final User user) {
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable List<User> getAll() {
        return entityManager
                .createQuery("SELECT u FROM User u", User.class)
                .getResultList();
    }

    @Override
    public @Nullable User getByLogin(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT u FROM User u WHERE u.login = ?1", User.class)
                .setParameter(1, login)
                .getSingleResult();
    }

    @Override
    public @Nullable User get(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT u FROM User u WHERE u.id = ?1", User.class)
                .setParameter(1, id)
                .getSingleResult();
    }

    @Override
    public void delete(@NotNull final String id) {
        @Nullable final User user = get(id);
        if (user == null) return;
        entityManager.getTransaction().begin();
        entityManager.remove(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable User existsUser(@NotNull final String login, @NotNull final String passwordHash) {
        @Nullable final User user = entityManager
                .createQuery("SELECT u FROM User u WHERE u.login = ?1 AND u.passwordHash = ?2", User.class)
                .setParameter(1, login)
                .setParameter(2, passwordHash)
                .getSingleResult();
        return user;
    }

    @Override
    public void changePassword(@NotNull final String id, @NotNull final String newPasswordHash) {
        entityManager.getTransaction().begin();
        entityManager.createQuery("UPDATE User SET User.passwordHash = ?1 where id = ?2")
                .setParameter(1, newPasswordHash)
                .setParameter(2, id)
                .executeUpdate();
    }
}
