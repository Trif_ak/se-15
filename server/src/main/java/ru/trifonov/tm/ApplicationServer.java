package ru.trifonov.tm;

import ru.trifonov.tm.bootstrap.*;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class ApplicationServer {
    public static void main(final String[] args) throws Exception {
        SeContainerInitializer.newInstance()
                .addPackages(ApplicationServer.class).initialize()
                .select(Bootstrap.class).get().start();
    }
}